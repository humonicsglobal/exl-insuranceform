const nconf = require('nconf');
const replaceall = require("replaceall");
const fs = require('fs');
const path = require('path');

function Config() {
    nconf.argv().env();

    process.chdir(path.join(__dirname, '../config/'));
    const relativePath = process.cwd();

    //const relativePath = './FrameworkService/v1/common/config/';  //Visual Studio Code (using run)
    // const relativePath ='../../botframework/FrameworkService/v1/common/config/';                     //WebStorm / through Terminal

    const environment = nconf.get('NODE_ENV') || 'development';
    const sampleFile = relativePath + "/sample.json";

    if(environment==="production"){
        //Read production.json file.
        const prodFile = relativePath + "/production.json";
        let prodData = fs.readFileSync(prodFile,'utf8');

        prodData = replaceall('$INSURANCE_PORT', nconf.get('INSURANCE_PORT'),prodData);
        prodData = replaceall('$INSURANCE_HOST_URL', nconf.get('INSURANCE_HOST_URL'),prodData);


        prodData = replaceall('$INSURANCE_DB_URL', nconf.get('INSURANCE_DB_URL'),prodData);

        //Write config data to sample file
        fs.writeFileSync(sampleFile, prodData);
    }else{
        //Read development.json file.
        const devFile = relativePath + "/development.json";
        const devData = fs.readFileSync(devFile,'utf8');

        //Write config data to sample file
        fs.writeFileSync(sampleFile, devData);
    }

    nconf.file(environment, relativePath + '/sample.json');
    nconf.file('default', relativePath + '/default.json');
}

Config.prototype.get = function(key) {
    return nconf.get(key);
};

module.exports = new Config();
