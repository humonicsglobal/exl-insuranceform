const config = require('../common/utils/config-util');
const express = require('express');


const router = express.Router();

const InsuranceFormController = require('./insurance-form-controller');
const insuranceFormController = new InsuranceFormController(config);



router.post('/fetch', insuranceFormController.fetchFormData);
router.post('/create', insuranceFormController.createFormData);




module.exports = router;
