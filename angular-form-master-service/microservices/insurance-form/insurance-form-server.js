process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


/****************Helper and services***************************/
const config = require('../common/utils/config-util');


/******************* ROUTES ************************************/
const insurance = require('./insurance-form-routes');


/*******************NPM Libraries*****************************/
const path = require('path');
const express = require('express');
const helmet = require('helmet');
const app = express();
const bodyParser = require("body-parser");
const cors = require('cors');


/****************************Middleware***********************************************/
app.use(helmet());
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({limit: '20mb', extended: true}));
app.use(express.static(__dirname));
// app.use(cors());

/*************************************************Routes********************************************/



app.use(express.static(path.join(__dirname,'../../../angular-form-master-client/dist/ivrform')));




app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../../../angular-form-master-client/dist/ivrform/index.html'));
});



app.use('/insurance-form',insurance);


/*******************************Server Hosting*****************************************/
const server = app.listen(config.get('insurance:server:port'), function() {
  console.log('Node server is running..'+config.get('insurance:server:port'));
});


