
const InsuranceFormService = require('./insurance-form-service');



class InsuranceFormController{

    constructor(config) {

        this.config=config;
        this.insuranceFormService=new InsuranceFormService(config);
        this.createFormData = this.createFormData.bind(this);
        // this.updateFormData = this.updateFormData.bind(this);
        this.fetchFormData = this.fetchFormData.bind(this);

    }


    async createFormData(req,res){
        console.log("createFormData")

        let json = {};
        try {
            let formData=req.body.contactFormData;
            let mode =req.body.mode;
            let type=req.body.type;


            let result =await  this.insuranceFormService.createFormData(formData,type,mode);


            res.send({"response":"SUCCESS", "formId": result});
            res.end();
        } catch (e) {
            console.log('Error!', e);

            json = {"response": "FAILURE", "message":"Error Occurred"};

            res.status(500).send(json);
            res.end();
        }

    }



    async fetchFormData(req,res){
        let json = {};
        try {

            console.log(req.body)

            let phoneNumber = req.body.phone;
            const result = await this.insuranceFormService.fetchFormData(phoneNumber);
            if(result.length!==0){


                json = {"response": "SUCCESS",  "data": result[0] ,"match":true};
            }else{

                json = {"response": "SUCCESS",  "match":false };
            }




                res.send(json);
                res.end();
        } catch (e) {
            console.log('Error!', e);

            json = {"response": "ERROR", "message":"error occured"};

            res.send(json);
            res.end();
        }





    }


    // async updateFormData(req,res){
    //
    // }

}
module.exports=InsuranceFormController;
