const MongoDB=require("../common/database/mongodb")


class InsuranceFormService{

    constructor(config) {
     this.mongoDB=new MongoDB(config.get('insurance:mongodb:adminUrl'))
        this.createFormData = this.createFormData.bind(this);

        this.fetchFormData = this.fetchFormData.bind(this);
    }

    async createFormData(form,type,mode){


        try {

            let data;
            let result;



            if(mode==="add" && type==="next" && form.page===0){
                result = await this.mongoDB.createRecord("insurance_form_data",form);
            }
            else if(mode==="add" && type==="submit"){

                const formId = await this.mongoDB.fetchSequenceValue("formId");
                form.formId = `#${formId}`;
                let query = {phone: form.phone};
                let values = {$set: form};
                result = await this.mongoDB.updateRecord("insurance_form_data", query, values);
            }
            else if((form.page!==0&& type==="next")||mode==="edit"){
                let query = {phone: form.phone};
                let values = {$set: form};
                console.log(JSON.stringify(values))
                console.log(JSON.stringify(query))
                result = await this.mongoDB.updateRecord("insurance_form_data", query, values);

            }




            return form.formId;


        } catch(err) {
            throw new Error(err)
        }



    }



    async fetchFormData(phoneNumber){

        try {
            let query = {phone : phoneNumber};


            let result = await this.mongoDB.findRecord("insurance_form_data",query);



            return result;
        } catch(err) {
            throw new Error(err)
        }



    }


}


module.exports=InsuranceFormService;
