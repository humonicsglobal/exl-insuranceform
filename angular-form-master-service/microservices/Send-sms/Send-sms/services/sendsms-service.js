

const AWS = require('aws-sdk');
class SendSmsService{

    constructor(config) {

        this.config=config;
        this.sendSms=this.sendSms.bind(this)
    }


    async sendSms(phone_number, link) {

        AWS.config = this.config.get('sendSms:AWS-Conf')
        if (phone_number.length === 10) {
             phone_number = "+91" + phone_number
        }
        console.log("_______", phone_number)
        let params = {
            Message: `we will guide you in the process please click on the link to proceed futher ${link}`,
            PhoneNumber:  phone_number,
            MessageAttributes: {
                'AWS.SNS.SMS.SenderID': {
                    'DataType': 'String',
                    'StringValue': 'HUMIND1'
                }
            }

        }

        let publishTextPromise = new AWS.SNS({apiVersion: '2010-03-31'}).publish(params).promise();
        publishTextPromise.then(
            function(data) {
                console.log("MessageID is " + data.MessageId);
            }).catch(
            function(err) {
                console.error(err, err.stack);
            });


    }


}
module.exports=SendSmsService