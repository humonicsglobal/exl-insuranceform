
const AWS = require('aws-sdk');
const SendSmsService = require('../services/sendsms-service');

class SendSmsController {
    constructor(config) {
        this.config=config
        this.sendSmsService = new SendSmsService( this.config);
        this.smsLinkData = this.smsLinkData.bind(this)
    }

    async smsLinkData(req, res) {

        try {

            AWS.config = this.config.get('AWS-Conf')
            let reqBody = req.body;
            console.log("req.body")
            console.log(req.body)
            console.log("req.body")
            console.log("req.body.parameters")
            console.log(req.body.parameters)
            console.log("req.body.parameters")
    //      req.body.parameters.userIdentity="8413004593"
           if(reqBody.parameters) {
               let channel_id =reqBody.parameters.channel_id
               let phone_number =reqBody.parameters.userIdentity
               let sessionId = reqBody.session_id
               let botKey = reqBody.bot_key
               // let link = this.config.get('sendSms:messageurl:url') + sessionId + '/' + channel_id + '/' + phone_number + '/' + botKey;
               let link = `http://15.207.33.93:5000/form?channelId=${channel_id}&phone=${phone_number}`
               // let messageBody = req.body.parameters.msg
               let messageBody = "Please click on this link to fill the form #link";
               console.log("phone_number")
               console.log(phone_number)
               console.log("phone_number")
               if (messageBody.includes("#link")) {
                   messageBody = messageBody.replace("#link", link)
               }
               await this.sendSmsService.sendSms(phone_number, messageBody)
               res.send({
                   response: {
                       template_type: "text",
                       text: "A link has been sent via sms on your phone number"
                   },
                   parameters: {visual_ivr: 1},
                   followup_event: "",
                   entity_name: ""
               })
               res.end();
           }

        } catch (e) {

            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send("MessageUtil.getMessage().Unable_Fetch");
            res.end();
        }
    }

}

module.exports=SendSmsController