const config = require('../../../common/utils/config-util');
const express = require('express');
const router = express.Router();
const SendSmsController = require('../controllers/sendsms-controller');
const sendSmsController = new SendSmsController(config);


router.post('/sendSms',sendSmsController.smsLinkData);


module.exports=router