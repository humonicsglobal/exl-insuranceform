import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {FormService} from "../services/form.service";
import { Observable, timer } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

import Swal from 'sweetalert2';
declare var $:any
import * as interfaces from "./dropdown_interface"
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public contactForm: FormGroup
  public contactFormData:any ={ };
  public subReasonForContact: any[] = [];
  public subReasonForContactBool: boolean = false;
  public formSubmitted: boolean = false;
  public showError1: boolean = false;
  public showError2: boolean = false;
  public showError3: boolean = false;
  public showError4: boolean = false;
  public showError5: boolean = false;
  public showError6: boolean = false;
  public showError7: boolean = false;
  public explink: boolean = false;
  public showList: boolean = true;
  public count = 0;
  public missedTimeFromWorkVal = interfaces.MissedTimefromWork
  public USStates = interfaces.USStates
  public admissionTypeVal = interfaces.AdmissionType
  public specialityVal = interfaces.Speciality
  public phoneNumber:any;
  public mode:string="add";
  public highlight:Array<any>=[];
  public lastPageMessage:string="";
  public channelId:any;
  public voiceResApi:boolean=false;
  public timeout=null;
  public editDetails:boolean=false;
  public transferToAgent:boolean=false;
  public getCallDisconnectNotification;
  public nextEventCounter: Array<number> = [0, 0, 0, 0, 0, 0];

  autoTicks = false;
  disabled = false;
  invert = false;
  max = 100;
  min = 0;
  showTicks = false;
  step = 1;
  thumbLabel = false;
  value = 0;
  vertical = false;
  tickInterval = 1;

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }

    return 0;
  }


  @ViewChild('appDirective') el: ElementRef;


  constructor(private fb:FormBuilder, private renderer: Renderer2,private formService:FormService,private route:ActivatedRoute) {
    console.log(this.route.snapshot)
    console.log('Called Constructor');
    this.route.queryParams.subscribe(params => {
      this.phoneNumber = params['phone'];
      // this.phoneNumber = 7651847769
      this.channelId = params['channelId'];
      console.log(this.phoneNumber,this.channelId);
    });
  }
slider1
slider2
rangeVal() {
  this.slider1=$('#myRange').val()
  if(this.slider2){
    this.voiceResponse(`aapakee rn raashi rs.${this.slider1}  aur ${this.slider2} maheenon ke lie rn kee avadhi. kee prosesing fees ke saath rs. 14000 aur 11% kee byaaj dar. jaaree rakhane ke lie aage dabaen.`)
  }
}
rangeVal1() {
  this.slider2=$('#myRange1').val()
  if(this.slider1){
    this.voiceResponse(`aapakee rn raashi rs.${this.slider1}  aur ${this.slider2} maheenon ke lie rn kee avadhi. kee prosesing fees ke saath rs. 14000 aur 11% kee byaaj dar. jaaree rakhane ke lie aage dabaen.`)
  }
  
}
imageChange() {
  this.voiceResponse("jaaree rakhane ke lie aage dabaen.")
}
totalper
getEIR (n1,n2) {
  this.totalper = n1/n2
  if(isNaN(this.totalper)){
    this.totalper=0;
}
  var n = this.totalper.toFixed(2);
  return n
}
  ngOnInit() {
    console.log("inside ngonint")
    if (true) {
      // this.handlecallDisconnection();
      console.log("line 113")
      this.initializeForm()
      console.log("line 115")
      this.formService.voiceApi({"channel_id": this.channelId, "msg": ""}).subscribe(
        (result: any) => {
          console.log(result)
          if (result.status !== 404) {
            console.log("got api voice welcome line 120")
            this.voiceResApi = true
            let data = {phone: this.phoneNumber};
            this.formService.fetchFormData(data).subscribe((result: any) => {

              if (result.response === "SUCCESS") {

                console.log(result.match)
                if (result.match) {
                  console.log("found")
                  console.log(result.data.page)
                  this.voiceResponse(interfaces.openingmessages[result.data.page + 1])

                  this.initializeForm();
                  this.presetForm(result.data);


                  if (result.data.page === 6) {
                    console.log(this.editDetails);
                    setTimeout(()=>{if(!this.editDetails){this.voiceResponse(`Your form has been submitted successfully. Your reference number is ${result.data.formId}`)}},5000);

                  }

                  if (result.data.page < 6) {
                    this.count = result.data.page + 1
                    for (let i = result.data.page; i > -1; i--) {
                      this.highlight.push(i)
                    }


                  } else {
                    this.count = 7;
                    this.lastPageMessage = `Thank you!!Form Submitted Successfully!!\n Form Id is ${result.data.formId}`
                  }


                } else {

                  console.log("not found")
                  this.voiceResponse("apane aavedan ke saath shuroo karane ke lie, mujhe aapake rn aavedan ko poora karane ke lie kuchh vivaranon kee aavashyakata hogee krpaya apanee rn raashi aur rn jama raashi ka chayan karen")
                  this.initializeForm();

                  this.mode = "add";
                }

                // this.handlecallDisconnection();
              } else {


                Swal.fire(`ERROR OCCURRED`);
              }

            }, error => {


              Swal.fire(`ERROR OCCURRED`);
            });


          } else {



            this.explink = true;
          }

        },
        err => {

          Swal.fire(JSON.stringify(err));
        })

    }
  }


public agentTransfer(){

  this.transferToAgent=true;
  this.voiceResponse('transferagent');


}

  public editDetail(){
    this.editDetails=true;
    this.mode="edit";
    this.count=0;
    this.voiceResponse("In this form I will be requiring few inputs from you to fill insurance form. Lets begin with your basic details and click Next.")
  }

  public handlecallDisconnection(){
    
    this.formService.initSocket(this.channelId,this.phoneNumber);

    this.getCallDisconnectNotification= this.formService.getCallDisconnectNotification().subscribe((data:any) => {
      if(data){


        console.log(data)
        this.explink = true;
        this.formService.removeSocket();
        // this.getCallDisconnectNotification.unsubscribe();
      }
    });
  }



  public onKeySearch(event: any, pageNumber) {
    clearTimeout(this.timeout);
    this.timeout = setTimeout( ()=> {

      if( this.nextEventCounter[pageNumber]<1){
      this.voiceResponse('click Next to continue')
      this.nextEventCounter[pageNumber]++;
      };

    }, 1500);
  }

  public onCalendarInput(event: any, msg) {
    console.log(event);
    console.log(event.value);

    clearTimeout(this.timeout);
    this.timeout = setTimeout( ()=> {


      if(event.value===null){this.voiceResponse("date format is incorrect. Please enter in mmonth/date/year format")}
      else{this.voiceResponse(msg)}


    }, 0);
  }





  public voiceResponse(message){

    console.log(message)
    this.formService.voiceApi({"channel_id":this.channelId,"msg":message}).subscribe((result:any)=>{

      console.log(result)
      if(result.status!==404) {




      }else{
        this.explink=true

        this.voiceResApi=false;

      }




    },err=>{

      this.voiceResApi=false;
      Swal.fire( `ERROR OCCURRED`);


    })



  }

  public countValueChange(num) {
    this.count = num
  }
  public transition(num) {
    if(num===0){
      if( !this.contactForm.value.firstname|| !this.contactForm.value.lastname || !this.contactForm.value.dob  ||
        !this.contactForm.value.employeeId  || !this.contactForm.value.taxId  ){
        this.formSubmitted=true;
        this.showError1=true;
        this.voiceResponse("all fields are mandatory please fill the highlited field to contnue ")

      }else{
        this.showError1=false;
        this.formSubmitted=false;

        this.voiceResponse('Thanks for sharing your basic details. May I know for whom you are filing Claim today? You can select options available.');
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }
    if(num===1){
      console.log('inside 1')
      if(!this.contactForm.value.missedTimeFromWork ){
        this.showError1=true;
        this.voiceResponse("All fiels are mandatory. Please fill all the details ")

      }else{
        this.showError1=false;
        this.voiceResponse('Next, I will be needing information about claimant! and do remember to scroll down to fill complete details.')
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }
    if(num===2){
      if( !this.contactForm.controls.claimantDetails.valid || !this.contactForm.controls.addressDetails.valid || !this.contactForm.value.jobTitle
        || !this.contactForm.value.descriptionOfWork ||  !this.contactForm["controls"]["addressDetails"]["controls"]["moreDetails"].valid){
        this.showError1=true;
        this.formSubmitted=true;
        this.voiceResponse("All fiels are mmandatory. Please fill all the details ")

      }else{
        this.showError1=false;
        this.formSubmitted=false;
        this.voiceResponse('Thank you! Next, I would require information on your employemnt schedule! Please select from given options about your employment schedule and do remember to scroll down to fill complete details.')
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }
    if(num===3){
      if(!this.contactForm.value.employmentSchedule || !this.contactForm.value.workForOtherEmployeeStatus || !this.contactForm.value.workLiveInSameState
        ||( this.contactForm.value.workForOtherEmployeeStatus ==="Yes" && this.contactForm["controls"]["workForOtherEmployee"].invalid)){
        this.showError1=true;
        this.formSubmitted=true;
        this.voiceResponse("All fiels are mmandatory. Please fill all the details ")

      }else{
        this.showError1=false;
        this.formSubmitted=false;
        this.voiceResponse('Thank you for sharing employment details. Now I will continue with details on your illness and Work Leave.To start with this section, is your accident or illness work related?')
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }
    if(num===4){
      if( !this.contactForm.value.accidentWorkRelated ||!this.contactForm.value.lastDateWorked
        ||!this.contactForm.value.leaveStartDate ||!this.contactForm.value.returnToWorkDateStatus ||!this.contactForm.value.residentStateOnDisabilityDate  ||
        ( this.contactForm.value.accidentWorkRelated ==="Yes" && !this.contactForm.value.claimFilled )||
        ( this.contactForm.value.returnToWorkDateStatus ==="Yes" && !this.contactForm.value.returnToWorkDate )){
        this.showError1=true;
        this.formSubmitted=true;

        this.voiceResponse("All fiels are mmandatory. Please fill all the details ")

      }else{
        this.showError1=false;
        this.formSubmitted=false;
        this.voiceResponse('Thank you for sharing your illness and Work leave details. Now, in the second last section I will be requiring your hospital visit details. Have you visited to any hospital yet? Select yes or no from the given option')
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }
    if(num===5){
      if( !this.contactForm.value.visitToHospital || (this.contactForm['controls']['visitToHospital']['value'] ==='Yes' && this.contactForm['controls']['visitToHospitalDetails'].invalid)){
        this.showError1=true;
        this.formSubmitted=true;
        this.voiceResponse("All fiels are mmandatory. Please fill all the details ")

      }else{
        this.showError1=false;
        this.formSubmitted=false;
        this.voiceResponse('In the last section, I will be taking details about physician. Please scroll down  to fill complete details.')
        this.highlight.push(num)
        this.createFormData("next")
        this.count = this.count+1;
      }
    }




  }


  public previous(){
    this.count=this.count-1;
  }


  public toggle(num) {
    this.count = num;
  }

  public initializeForm() {
    this.contactForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      dob: [ {disabled: true}, Validators.required],
      employeeId: ['', Validators.required],
      taxId: ['', Validators.required],
      contactOption: ['', Validators.required],
      reason: ['', Validators.required],
      missedTimeFromWork:['', Validators.required],
      claimantDetails: this.fb.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        EmpID:['', Validators.required],
        DOB: ['', Validators.required],
        SSN: ['', Validators.required],
        preferredLang: ['', Validators.required],
        email : ['', Validators.required],
        primaryPhone :['', Validators.required],
        maritalStatus: ['', Validators.required],
        registerForIservices: [''],
        cellPhoneNumber: [''],
        registerForTaxAlerts: ['', Validators.required]
      }),
      addressDetails: this.fb.group({
        addressType: ['', Validators.required],
        moreDetails : this.fb.group({
          addr1: ['', Validators.required],
          addr2: [''],
          city: ['', Validators.required],
          country: ['', Validators.required],
          state: ['', Validators.required],
          zip : ['', Validators.required],
        })
      }),
      jobTitle: ['', Validators.required],
      descriptionOfWork: ['', Validators.required],
      employmentSchedule: ['', Validators.required],
      workForOtherEmployeeStatus: ['', Validators.required],
      workForOtherEmployee: this.fb.group({
        employerName: ['', Validators.required],
        contactName: ['', Validators.required],
        contactNumber : ['', Validators.required],
        jobTitle : ['', Validators.required],
      }),
      workLiveInSameState: ['', Validators.required],
      accidentWorkRelated: ['', Validators.required],
      claimFilled: ['', Validators.required],
      lastDateWorked: ['', Validators.required],
      leaveStartDate: ['', Validators.required],
      returnToWorkDateStatus: ['', Validators.required],
      returnToWorkDate : ['', Validators.required],
      lastDayWorked: ['', Validators.required],
      residentStateOnDisabilityDate: ['', Validators.required],
      visitToHospital: ['', Validators.required],
      visitToHospitalDetails: this.fb.group({
        admissionType: ['', Validators.required],
        admissionDate: ['', Validators.required],
        hospitalName: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        tel: ['', Validators.required],
        dischargeDate : ['', Validators.required],
      }),
      physicianDetails: this.fb.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        organization:['', Validators.required],
        state: ['', Validators.required],
        contact: ['', Validators.required],
        fax: [''],
        speciality : ['', Validators.required],
        category :['', Validators.required],
        firstVisit: ['', Validators.required],
        nextVisit: ['', Validators.required],
        isAnotherPhysicianInvolved: ['', Validators.required]}),
      anotherPhysicanData: this.fb.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        organization:['', Validators.required],
        state: ['', Validators.required],
        contact: ['', Validators.required],
        fax: [''],
        speciality :['', Validators.required],
      })
    })
  }





  public presetForm(data) {

    if(data.contactOption==="Myself"){
      this.subReasonForContact = interfaces.Reason_for_contact_myself;
      this.subReasonForContactBool = true;

    }else if(data.contactOption==="Spouse"){
      this.subReasonForContact = interfaces.Reason_for_contact_spouse;
      this.subReasonForContactBool = true;

    }else if(data.contactOption==="Child"){
      this.subReasonForContact = interfaces.Reason_for_child;
      this.subReasonForContactBool = true;
    }
    this.contactForm.patchValue({
      firstname: data.firstname,
      lastname: data.lastname,
      dob: data.dob,
      employeeId: data.employeeId,
      taxId: data.taxId,
      phone: data.phone,
      contactOption: data.contactOption,
      reason:  data.reason,
      missedTimeFromWork: data.missedTimeFromWork,
      claimantDetails: {
        firstname: data.claimantDetails.firstname,
        lastname: data.claimantDetails.lastname,
        EmpID: data.claimantDetails.EmpID,
        DOB:data.claimantDetails.DOB,
        SSN:data.claimantDetails.SSN,
        preferredLang: data.claimantDetails.preferredLang,
        email: data.claimantDetails.email,
        primaryPhone: data.claimantDetails.primaryPhone,
        maritalStatus: data.claimantDetails.maritalStatus,
        registerForIservices: data.claimantDetails.registerForIservices,
        cellPhoneNumber: data.claimantDetails.cellPhoneNumber,
        registerForTaxAlerts: data.claimantDetails.registerForTaxAlerts,
      },
      addressDetails: {
        addressType: data.addressDetails.addressType,
        moreDetails: {
          addr1: data.addressDetails.moreDetails.addr1,
          addr2: data.addressDetails.moreDetails.addr2,
          city: data.addressDetails.moreDetails.city,
          country: data.addressDetails.moreDetails.country,
          state:data.addressDetails.moreDetails.state,
          zip: data.addressDetails.moreDetails.zip,
        }
      },
      jobTitle: data.jobTitle,
      descriptionOfWork: data.descriptionOfWork,
      employmentSchedule: data.employmentSchedule,
      workForOtherEmployeeStatus: data.workForOtherEmployeeStatus,
      workForOtherEmployee: {
        employerName: data.workForOtherEmployee.employerName,
        contactName: data.workForOtherEmployee.contactName,
        contactNumber: data.workForOtherEmployee.contactNumber,
        jobTitle: data.workForOtherEmployee.jobTitle,
      },
      workLiveInSameState: data.workLiveInSameState,
      accidentWorkRelated: data.accidentWorkRelated,
      claimFilled: data.claimFilled,
      lastDateWorked: data.lastDateWorked,
      leaveStartDate:data.leaveStartDate,
      returnToWorkDateStatus: data.returnToWorkDateStatus,
      returnToWorkDate:data.returnToWorkDate,
      lastDayWorked:data.lastDayWorked,
      residentStateOnDisabilityDate: data.residentStateOnDisabilityDate,
      visitToHospital: data.visitToHospital,
      visitToHospitalDetails: {
        admissionType: data.visitToHospitalDetails.admissionType,
        admissionDate: data.visitToHospitalDetails.admissionDate,
        hospitalName: data.visitToHospitalDetails.hospitalName,
        city: data.visitToHospitalDetails.city,
        state: data.visitToHospitalDetails.state,
        tel: data.visitToHospitalDetails.tel,
        dischargeDate: data.visitToHospitalDetails.dischargeDate,
      },
      physicianDetails: {
        firstname: data.physicianDetails.firstname,
        lastname: data.physicianDetails.lastname,
        organization: data.physicianDetails.organization,
        state: data.physicianDetails.state,
        contact: data.physicianDetails.contact,
        fax: data.physicianDetails.fax,
        speciality:data.physicianDetails.speciality,
        category: data.physicianDetails.category,
        firstVisit:data.physicianDetails.firstVisit,
        nextVisit: data.physicianDetails.nextVisit,
        isAnotherPhysicianInvolved: data.physicianDetails.isAnotherPhysicianInvolved},
      anotherPhysicanData: {
        firstname: data.anotherPhysicanData.firstname,
        lastname:  data.anotherPhysicanData.lastname,
        organization:  data.anotherPhysicanData.organization,
        state: data.anotherPhysicanData.state,
        contact:  data.anotherPhysicanData.contact,
        fax:  data.anotherPhysicanData.fax,
        speciality:  data.anotherPhysicanData.speciality,
      }}
    );
  }

  public selectContactReason() {
    console.log(this.contactForm.controls['contactOption']['value'])
    if(this.contactForm.controls['contactOption']['value'] === 'Myself'){
      this.subReasonForContact = interfaces.Reason_for_contact_myself;
      this.subReasonForContactBool = true;
      console.log(this.subReasonForContact)
      this.contactForm.patchValue({

        claimantDetails: {
          firstname: this.contactForm.controls['firstname']['value'],
          lastname: this.contactForm.controls['lastname']['value'],
          EmpID: this.contactForm.controls['employeeId']['value'],
          DOB: this.contactForm.controls['dob']['value']}
      });



    }
    else if(this.contactForm.controls['contactOption']['value'] === 'Spouse'){
      this.subReasonForContact = interfaces.Reason_for_contact_spouse;
      this.subReasonForContactBool = true;}
    else  if(this.contactForm.controls['contactOption']['value'] === 'Child'){
      this.subReasonForContact = interfaces.Reason_for_child;
      this.subReasonForContactBool = true;
    }
  }



  public submitForm(){



    if((!this.contactForm.controls.physicianDetails.valid )||
      (this.contactForm['controls']['physicianDetails']['controls']['isAnotherPhysicianInvolved']['value'] ==='Yes' && this.contactForm['controls']['anotherPhysicanData'].invalid)){
      this.showError1=true;
      this.formSubmitted=true;
      this.voiceResponse("All fiels are mmandatory. Please fill all the details  to submit form")

    }else{
      this.editDetails=false;
      this.showError1=false;
      this.formSubmitted=false;
      this.createFormData("submit");
      this.highlight.push(6);
      this.count=this.count+1;
      this.showList=false;


    }


  }


  public createFormData(type) {
    this.contactFormData = {
      firstname: this.contactForm.controls['firstname']['value'],
      lastname: this.contactForm.controls['lastname']['value'],
      dob: this.contactForm.controls['dob']['value'],
      employeeId: this.contactForm.controls['employeeId']['value'],
      taxId: this.contactForm.controls['taxId']['value'],
      phone: this.phoneNumber,
      page:this.count,
      contactOption: this.contactForm.controls['contactOption']['value'],
      reason: this.contactForm.controls['reason']['value'],
      missedTimeFromWork: this.contactForm.controls['missedTimeFromWork']['value'],
      claimantDetails: {
        firstname: this.contactForm.controls['claimantDetails']['controls']['firstname']['value'],
        lastname: this.contactForm.controls['claimantDetails']['controls']['lastname']['value'],
        EmpID: this.contactForm.controls['claimantDetails']['controls']['EmpID']['value'],
        DOB: this.contactForm.controls['claimantDetails']['controls']['DOB']['value'],
        SSN: this.contactForm.controls['claimantDetails']['controls']['SSN']['value'],
        preferredLang: this.contactForm.controls['claimantDetails']['controls']['preferredLang']['value'],
        email: this.contactForm.controls['claimantDetails']['controls']['email']['value'],
        primaryPhone: this.contactForm.controls['claimantDetails']['controls']['primaryPhone']['value'],
        maritalStatus: this.contactForm.controls['claimantDetails']['controls']['maritalStatus']['value'],
        registerForIservices: this.contactForm.controls['claimantDetails']['controls']['registerForIservices']['value'],
        cellPhoneNumber: this.contactForm.controls['claimantDetails']['controls']['cellPhoneNumber']['value'],
        registerForTaxAlerts: this.contactForm.controls['claimantDetails']['controls']['registerForTaxAlerts']['value']
      },
      addressDetails: {
        addressType: this.contactForm.controls['addressDetails']['controls']['addressType']['value'],
        moreDetails: {
          addr1: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['addr1']['value'],
          addr2: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['addr2']['value'],
          city: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['city']['value'],
          country: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['country']['value'],
          state: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['state']['value'],
          zip: this.contactForm.controls['addressDetails']['controls']['moreDetails']['controls']['zip']['value'],
        }
      },
      jobTitle: this.contactForm.controls['jobTitle']['value'],
      descriptionOfWork: this.contactForm.controls['descriptionOfWork']['value'],
      employmentSchedule: this.contactForm.controls['employmentSchedule']['value'],
      workForOtherEmployeeStatus: this.contactForm.controls['workForOtherEmployeeStatus']['value'],
      workForOtherEmployee: {
        employerName: this.contactForm.controls['workForOtherEmployee']['controls']['employerName']['value'],
        contactName: this.contactForm.controls['workForOtherEmployee']['controls']['contactName']['value'],
        contactNumber: this.contactForm.controls['workForOtherEmployee']['controls']['contactNumber']['value'],
        jobTitle: this.contactForm.controls['workForOtherEmployee']['controls']['jobTitle']['value'],
      },
      workLiveInSameState: this.contactForm.controls['workLiveInSameState']['value'],
      accidentWorkRelated: this.contactForm.controls['accidentWorkRelated']['value'],
      claimFilled: this.contactForm.controls['claimFilled']['value'],
      lastDateWorked: this.contactForm.controls['lastDateWorked']['value'],
      leaveStartDate: this.contactForm.controls['leaveStartDate']['value'],
      returnToWorkDateStatus: this.contactForm.controls['returnToWorkDateStatus']['value'],
      returnToWorkDate: this.contactForm.controls['returnToWorkDate']['value'],
      lastDayWorked: this.contactForm.controls['lastDayWorked']['value'],
      residentStateOnDisabilityDate: this.contactForm.controls['residentStateOnDisabilityDate']['value'],
      visitToHospital: this.contactForm.controls['visitToHospital']['value'],
      visitToHospitalDetails: {
        admissionType: this.contactForm.controls['visitToHospitalDetails']['controls']['admissionType']['value'],
        admissionDate: this.contactForm.controls['visitToHospitalDetails']['controls']['admissionDate']['value'],
        hospitalName: this.contactForm.controls['visitToHospitalDetails']['controls']['hospitalName']['value'],
        city: this.contactForm.controls['visitToHospitalDetails']['controls']['city']['value'],
        state: this.contactForm.controls['visitToHospitalDetails']['controls']['state']['value'],
        tel: this.contactForm.controls['visitToHospitalDetails']['controls']['tel']['value'],
        dischargeDate: this.contactForm.controls['visitToHospitalDetails']['controls']['dischargeDate']['value']
      },
      physicianDetails: {
        firstname: this.contactForm.controls['physicianDetails']['controls']['firstname']['value'],
        lastname: this.contactForm.controls['physicianDetails']['controls']['lastname']['value'],
        organization: this.contactForm.controls['physicianDetails']['controls']['organization']['value'],
        state: this.contactForm.controls['physicianDetails']['controls']['state']['value'],
        contact: this.contactForm.controls['physicianDetails']['controls']['contact']['value'],
        fax: this.contactForm.controls['physicianDetails']['controls']['fax']['value'],
        speciality: this.contactForm.controls['physicianDetails']['controls']['speciality']['value'],
        category: this.contactForm.controls['physicianDetails']['controls']['category']['value'],
        firstVisit: this.contactForm.controls['physicianDetails']['controls']['firstVisit']['value'],
        nextVisit: this.contactForm.controls['physicianDetails']['controls']['nextVisit']['value'],
        isAnotherPhysicianInvolved: this.contactForm.controls['physicianDetails']['controls']['isAnotherPhysicianInvolved']['value']},
      anotherPhysicanData: {
        firstname: this.contactForm.controls['anotherPhysicanData']['controls']['firstname']['value'],
        lastname: this.contactForm.controls['anotherPhysicanData']['controls']['lastname']['value'],
        organization: this.contactForm.controls['anotherPhysicanData']['controls']['organization']['value'],
        state: this.contactForm.controls['anotherPhysicanData']['controls']['state']['value'],
        contact: this.contactForm.controls['anotherPhysicanData']['controls']['contact']['value'],
        fax: this.contactForm.controls['anotherPhysicanData']['controls']['fax']['value'],
        speciality: this.contactForm.controls['anotherPhysicanData']['controls']['speciality']['value'],
      }
    };

    let data={
      "contactFormData": this.contactFormData,
      "mode":this.mode,
      "type":type

    }

    console.log(data)
    console.log(this.editDetails)
    this.formService.createFormData(data).subscribe((result:any)=>{console.log(result)
      if(result.response === 'SUCCESS' ){

        if(this.mode==="add" && type==="next" && this.contactFormData.page===0 && !this.editDetails){

          console.log("created successfully")

        }
        else if((this.mode==="add" && type==="submit")){
          this.lastPageMessage=`Form Submitted Successfully!! \n Form Id is ${result.formId}`
console.log("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
          console.log(this.editDetails)

          setTimeout(()=>{if(!this.editDetails){this.voiceResponse(`Thank you for your time and sharing required information. Your reference number is ${result.formId}`)}},5000);


        }else if( this.mode==="edit" && type==="submit"){

          console.log("|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||")
          console.log(this.editDetails)
          setTimeout(()=>{if(!this.editDetails){this.voiceResponse(`Your form has been submitted successfully. Your reference number is ${result.data.formId}`); } },5000);
        }
        else if(this.contactFormData.page!==0&& type==="next"){


          console.log("updated successfully")
        }



      }else{
        Swal.fire( `ERROR OCCURRED`);
      }
    },err=>{Swal.fire( `ERROR OCCURRED`)});
  }


  isFieldValid(field: string) {
    return (
      this.contactForm.get(field).errors && this.contactForm.get(field).touched ||
      this.contactForm.get(field).untouched &&
      this.formSubmitted && this.contactForm.get(field).errors
    );
  }
  isFieldValid1(field: string) {
    return (
      this.contactForm.controls.claimantDetails.get(field).errors &&  this.contactForm.controls.claimantDetails.get(field).touched ||
      this.contactForm.controls.claimantDetails.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.claimantDetails.get(field).errors
    );
  }
  isFieldValid2(field: string) {
    return (
      this.contactForm.controls.addressDetails.get(field).errors &&  this.contactForm.controls.addressDetails.get(field).touched ||
      this.contactForm.controls.addressDetails.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.addressDetails.get(field).errors
    );
  }
  isFieldValid3(field: string) {
    return (
      this.contactForm["controls"]["addressDetails"]["controls"]["moreDetails"].get(field).errors &&  this.contactForm["controls"]["addressDetails"]["controls"]["moreDetails"].get(field).touched ||
      this.contactForm["controls"]["addressDetails"]["controls"]["moreDetails"].get(field).untouched &&
      this.formSubmitted &&  this.contactForm["controls"]["addressDetails"]["controls"]["moreDetails"].get(field).errors
    );
  }
  isFieldValid4(field: string) {
    return (
      this.contactForm.controls.workForOtherEmployee.get(field).errors &&  this.contactForm.controls.workForOtherEmployee.get(field).touched ||
      this.contactForm.controls.workForOtherEmployee.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.workForOtherEmployee.get(field).errors
    );
  }
  isFieldValid5(field: string) {
    return (
      this.contactForm.controls.visitToHospitalDetails.get(field).errors &&  this.contactForm.controls.visitToHospitalDetails.get(field).touched ||
      this.contactForm.controls.visitToHospitalDetails.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.visitToHospitalDetails.get(field).errors
    );
  }
  isFieldValid6(field: string) {
    return (
      this.contactForm.controls.physicianDetails.get(field).errors &&  this.contactForm.controls.physicianDetails.get(field).touched ||
      this.contactForm.controls.physicianDetails.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.physicianDetails.get(field).errors
    );
  }
  isFieldValid7(field: string) {
    return (
      this.contactForm.controls.anotherPhysicanData.get(field).errors &&  this.contactForm.controls.anotherPhysicanData.get(field).touched ||
      this.contactForm.controls.anotherPhysicanData.get(field).untouched &&
      this.formSubmitted &&  this.contactForm.controls.anotherPhysicanData.get(field).errors
    );
  }
}



