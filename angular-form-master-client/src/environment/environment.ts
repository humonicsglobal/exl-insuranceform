// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  // serverUrl: 'http://localhost:5000',
   serverUrl: 'http://15.207.33.93:5000',
  //  serverUrl: 'https://ivr.humonics.ai/ivr-visual/',
  socketConfig: {
    url: 'http://13.235.175.133:3000',
    // options: {path: '/chat/socket.io'}
  }
 };


// export const environment = {
//   // production : true,
//   // serverUrl: 'http://localhost:5000',
//   serverUrl: 'https://dev.humonics.ai',
//   socketConfig: {
//     url: 'ivr.humonics.ai/',
//     // url: 'http://13.235.175.133:3000',
//     // options: {path: '/chat/socket.io'}
//   }
// };


// export const environment = {
//   production: false,
//   // serverUrl: 'http://localhost:5000',
//   serverUrl: 'https://bot.humonics.ai',
//   socketConfig: {
//     url: 'http://13.235.175.133:3000/',
//     options: {path: '/chat/socket.io'}
//   }
// };

